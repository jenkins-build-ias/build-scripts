#!/usr/bin/groovy

def call(body) {
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def buildResult

    def isMasterBranch = env.BRANCH_NAME == 'master'
    def isDevelopBranch = env.BRANCH_NAME == 'develop'

    node {
        properties([
            disableConcurrentBuilds(),
            [
                $class: 'BuildDiscarderProperty',
                strategy: [
                    $class: 'LogRotator',
                    artifactDaysToKeepStr: '',
                    artifactNumToKeepStr: '5',
                    daysToKeepStr: '',
                    numToKeepStr: '10'
                ]
            ]
        ])

        stage('Checkout') {
            checkout scm
        }
        stage('Build') {
            sh "./gradlew clean build"
        }
    }
}
